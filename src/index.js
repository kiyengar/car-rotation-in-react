import React from "react";
import { render } from "react-dom";
import Rotation from "react-rotation";

// Rotataion component. Disaplay images from images folder by color and rims props
const Images = props => {
  return (
    <Rotation>
      {["front2", "front1", "back2", "back1", "side"].map((imageName, i) => (
        <img
          key={i}
          src={`/images/rims${props.rims}/${props.color}/${imageName}.png`}
        />
      ))}
    </Rotation>
  );
};

// Button component. Click on button change rims / color state value
const ToggleButton = props => {
  return (
    <div class="button-wrap">
      <button
        onClick={() => {
          props.callback();
        }}
      >
        {props.text}
      </button>
    </div>
  );
};

// Main app component. Renders images and buttons
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      color: "black",
      rims: 1,
      colors: ["black", "white", "blue"]
    };
  }
  // Callback toogles rims 1 / 2
  toggleRims() {
    this.setState(state => ({
      rims: state.rims === 1 ? 2 : 1
    }));
  }

  // Callback changes color to next color in state colors array
  changeColors() {
    let currentColorIndex = this.state.colors.findIndex(color => {
      return color == this.state.color;
    });
    this.setState(state => ({
      color: state.colors[currentColorIndex < 2 ? currentColorIndex + 1 : 0]
    }));
  }

  render() {
    return (
      <div>
        <div className="wrap images-wrap">
          <Images rims={this.state.rims} color={this.state.color} />
        </div>

        <div className=" flex wrap buttons-wrap">
          <ToggleButton
            callback={() => this.changeColors()}
            text="Change Color"
          />
          <ToggleButton callback={() => this.toggleRims()} text="Change Rims" />
        </div>
      </div>
    );
  }
}

// Render the app in the div #roor in index.html file
render(<App />, document.getElementById("root"));
